Pod::Spec.new do |s|

  s.name          = "ZCPDebugPlugin"
  s.version       = "1.0.0"
  s.author        = { "朱超鹏" => "z164757979@163.com" }
  s.license       = { :type => 'MIT', :file => 'LICENSE' }
  s.homepage      = "https://gitlab.com/Malygos/ZCPDebugTool"
  s.source        = { :git => "https://gitlab.com/Malygos/ZCPDebugTool.git", :tag => "#{s.version}" }
  s.summary       = "ZCPDebugPlugin."
  s.description   = <<-DESC
                       It`s a ZCPDebugPlugin.
                       DESC

  s.platform      = :ios, '9.0'
  s.ios.deployment_target = '9.0'
  s.module_name   = 'ZCPDebugPlugin'
  s.framework     = 'Foundation', 'UIKit'

  # source_files start
  s.subspec 'Log' do |ss|
    ss.source_files = 'ZCPDebugPlugin/Log/**/*.{h,m}'
    ss.dependency 'fishhook'
  end
  s.subspec 'CrashReport' do |ss|
    ss.source_files = 'ZCPDebugPlugin/CrashReport/**/*.{h,m}'
  end
  s.subspec 'SandboxBrowse' do |ss|
    ss.source_files = 'ZCPDebugPlugin/SandboxBrowse/**/*.{h,m}'
    ss.resource_bundles = {
      'ZCPDebugSandboxBrowse' => 'ZCPDebugPlugin/SandboxBrowse/Assets.xcassets'
    }
  end
  s.subspec 'UIStuckMonitor' do |ss|
    ss.source_files = 'ZCPDebugPlugin/UIStuckMonitor/**/*.{h,m}'
  end
  # source_files end
  
  s.dependency 'ZCPDebugTool'

end
