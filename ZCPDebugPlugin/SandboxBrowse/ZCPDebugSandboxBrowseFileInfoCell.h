//
//  ZCPDebugSandboxBrowseFileInfoCell.h
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZCPDebugSandboxBrowseFileInfoCell : UITableViewCell

+ (NSString *)reuseIdentifier;

@end

NS_ASSUME_NONNULL_END
