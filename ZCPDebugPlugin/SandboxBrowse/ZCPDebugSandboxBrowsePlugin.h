//
//  ZCPDebugSandboxBrowsePlugin.h
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/7.
//

#import <Foundation/Foundation.h>
#import <ZCPDebugTool/ZCPDebugPluginBaseClass.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZCPDebugSandboxBrowsePlugin : ZCPDebugPluginBaseClass

@end

NS_ASSUME_NONNULL_END
