//
//  ZCPDebugSandboxBrowseVC.h
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/8.
//

#import <UIKit/UIKit.h>
@class ZCPDebugSandboxBrowseFileInfo;

NS_ASSUME_NONNULL_BEGIN

@interface ZCPDebugSandboxBrowseVC : UIViewController

/// 是否沙盒根目录，默认为YES
@property (nonatomic, assign, getter=isHomeDirectory) BOOL homeDirectory;
/// 沙盒文件/目录信息
@property (nonatomic, strong) ZCPDebugSandboxBrowseFileInfo *fileInfo;

@end

NS_ASSUME_NONNULL_END
