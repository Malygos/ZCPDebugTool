//
//  ZCPDebugSandboxBrowseFileInfo.m
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/8.
//

#import "ZCPDebugSandboxBrowseFileInfo.h"
#import <QuickLook/QuickLook.h>

#define SandboxBrowseAssetsName @"ZCPDebugSandboxBrowse"

static NSDateFormatter *_dateFormatter;

@interface ZCPDebugSandboxBrowseFileInfo ()

@property (nonatomic, strong, readwrite) NSString *typeImageName;

@end

@implementation ZCPDebugSandboxBrowseFileInfo

- (instancetype)initWithFileURL:(NSURL *)URL {
    if (self = [super init]) {
        self.URL = URL;
        self.displayName = URL.lastPathComponent;
        self.attributes = [ZCPDebugSandboxBrowseFileInfo attributesWithFileURL:URL];
        
        if ([self.attributes.fileType isEqualToString:NSFileTypeDirectory]) {
            self.fileType = ZCPDebugSandboxBrowseFileTypeDirectory;
            self.filesCount = [ZCPDebugSandboxBrowseFileInfo contentCountOfDirectoryAtURL:URL];
            if ([URL isFileURL]) {
                NSString *dateStr = [[ZCPDebugSandboxBrowseFileInfo dateFormatter] stringFromDate:self.attributes.fileModificationDate];
                NSString *sizeStr = [ZCPDebugSandboxBrowseFileInfo sizeOfFolder:URL.path];
                self.modificationDateText = [NSString stringWithFormat:@"[%@] %@", dateStr, sizeStr];
            }
        } else {
            self.extension = URL.pathExtension;
            self.fileType = [ZCPDebugSandboxBrowseFileInfo fileTypeWithExtension:self.extension];
            self.filesCount = 0;
            //liman
            if ([URL isFileURL]) {
                NSString *dateStr = [[ZCPDebugSandboxBrowseFileInfo dateFormatter] stringFromDate:self.attributes.fileModificationDate];
                NSString *sizeStr = [ZCPDebugSandboxBrowseFileInfo sizeOfFile:URL.path];
                self.modificationDateText = [NSString stringWithFormat:@"[%@] %@", dateStr, sizeStr];
            }
        }
        
        if ([self.modificationDateText containsString:@"[] "]) {
            self.modificationDateText = [[self.modificationDateText mutableCopy] stringByReplacingOccurrencesOfString:@"[] " withString:@""];
        }
    }
    
    return self;
}

+ (NSDictionary<NSString *, id> *)attributesWithFileURL:(NSURL *)URL {
    NSDictionary<NSString *, id> *attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:URL.path error:nil];
    
    return attributes;
}

+ (NSMutableArray<ZCPDebugSandboxBrowseFileInfo *> *)contentsOfDirectoryAtURL:(NSURL *)URL {
    NSMutableArray *fileInfos = [NSMutableArray array];
    BOOL isDir = NO;
    BOOL isExists = [[NSFileManager defaultManager] fileExistsAtPath:URL.path isDirectory:&isDir];
    if (isExists && isDir) {
        NSArray<NSString *> *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:URL.path error:nil];
        if ([contents count] > 0) {
            for (NSString *name in contents) {
                ZCPDebugSandboxBrowseFileInfo *fileInfo = [[ZCPDebugSandboxBrowseFileInfo alloc] initWithFileURL:[URL URLByAppendingPathComponent:name]];
                [fileInfos addObject:fileInfo];
            }
        }
    }
    return fileInfos;
}

+ (NSUInteger)contentCountOfDirectoryAtURL:(NSURL *)URL {
    NSUInteger count = 0;
    BOOL isDir = NO;
    BOOL isExists = [[NSFileManager defaultManager] fileExistsAtPath:URL.path isDirectory:&isDir];
    if (isExists && isDir) {
        NSArray<NSString *> *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:URL.path error:nil];
        count = contents.count;
    }
    return count;
}

#pragma mark - utils

+ (NSString *)sizeOfFolder:(NSString *)folderPath {
    NSArray *folderContents = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
    
    __block unsigned long long int folderSize = 0;
    
    [folderContents enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:[folderPath stringByAppendingPathComponent:obj] error:nil];
        folderSize += [[fileAttributes objectForKey:NSFileSize] intValue];
    }];
    NSString *folderSizeStr = [NSByteCountFormatter stringFromByteCount:folderSize countStyle:NSByteCountFormatterCountStyleFile];
    return folderSizeStr;
}

+ (NSString *)sizeOfFile:(NSString *)filePath {
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil];
    NSInteger fileSize = [[fileAttributes objectForKey:NSFileSize] integerValue];
    NSString *fileSizeString = [NSByteCountFormatter stringFromByteCount:fileSize countStyle:NSByteCountFormatterCountStyleFile];
    return fileSizeString;
}

+ (NSDateFormatter *)dateFormatter {
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss.SSS";
    }
    return _dateFormatter;
}

+ (ZCPDebugSandboxBrowseFileType)fileTypeWithExtension:(NSString *)extension {
    ZCPDebugSandboxBrowseFileType type = ZCPDebugSandboxBrowseFileTypeUnknown;
    
    if (![extension isKindOfClass:NSString.class] || extension.length == 0) {
        return type;
    }
    
    NSDictionary *map = @{
        // Image
        @"jpg": @(ZCPDebugSandboxBrowseFileTypeJPG),
        @"png": @(ZCPDebugSandboxBrowseFileTypePNG),
        @"gif": @(ZCPDebugSandboxBrowseFileTypeGIF),
        @"svg": @(ZCPDebugSandboxBrowseFileTypeSVG),
        @"bmp": @(ZCPDebugSandboxBrowseFileTypeBMP),
        @"tif": @(ZCPDebugSandboxBrowseFileTypeTIF),
        // Audio
        @"mp3": @(ZCPDebugSandboxBrowseFileTypeMP3),
        @"aac": @(ZCPDebugSandboxBrowseFileTypeAAC),
        @"wav": @(ZCPDebugSandboxBrowseFileTypeWAV),
        @"ogg": @(ZCPDebugSandboxBrowseFileTypeOGG),
        // Video
        @"mp4": @(ZCPDebugSandboxBrowseFileTypeMP4),
        @"avi": @(ZCPDebugSandboxBrowseFileTypeAVI),
        @"flv": @(ZCPDebugSandboxBrowseFileTypeFLV),
        @"midi": @(ZCPDebugSandboxBrowseFileTypeMIDI),
        @"mov": @(ZCPDebugSandboxBrowseFileTypeMOV),
        @"mpg": @(ZCPDebugSandboxBrowseFileTypeMPG),
        @"wmv": @(ZCPDebugSandboxBrowseFileTypeWMV),
        // Apple
        @"dmg": @(ZCPDebugSandboxBrowseFileTypeDMG),
        @"ipa": @(ZCPDebugSandboxBrowseFileTypeIPA),
        @"numbers": @(ZCPDebugSandboxBrowseFileTypeNumbers),
        @"pages": @(ZCPDebugSandboxBrowseFileTypePages),
        @"key": @(ZCPDebugSandboxBrowseFileTypeKeynote),
        // Google
        @"apk": @(ZCPDebugSandboxBrowseFileTypeAPK),
        // Microsoft
        @"doc": @(ZCPDebugSandboxBrowseFileTypeWord),
        @"docx": @(ZCPDebugSandboxBrowseFileTypeWord),
        @"xls": @(ZCPDebugSandboxBrowseFileTypeExcel),
        @"xlsx": @(ZCPDebugSandboxBrowseFileTypeExcel),
        @"ppt": @(ZCPDebugSandboxBrowseFileTypePPT),
        @"pptx": @(ZCPDebugSandboxBrowseFileTypePPT),
        @"exe": @(ZCPDebugSandboxBrowseFileTypeEXE),
        @"dll": @(ZCPDebugSandboxBrowseFileTypeDLL),
        // Document
        @"txt": @(ZCPDebugSandboxBrowseFileTypeTXT),
        @"rtf": @(ZCPDebugSandboxBrowseFileTypeRTF),
        @"pdf": @(ZCPDebugSandboxBrowseFileTypePDF),
        @"zip": @(ZCPDebugSandboxBrowseFileTypeZIP),
        @"7z": @(ZCPDebugSandboxBrowseFileType7z),
        @"cvs": @(ZCPDebugSandboxBrowseFileTypeCVS),
        @"md": @(ZCPDebugSandboxBrowseFileTypeMD),
        // Programming
        @"swift": @(ZCPDebugSandboxBrowseFileTypeSwift),
        @"java": @(ZCPDebugSandboxBrowseFileTypeJava),
        @"c": @(ZCPDebugSandboxBrowseFileTypeC),
        @"cpp": @(ZCPDebugSandboxBrowseFileTypeCPP),
        @"php": @(ZCPDebugSandboxBrowseFileTypePHP),
        @"json": @(ZCPDebugSandboxBrowseFileTypeJSON),
        @"plist": @(ZCPDebugSandboxBrowseFileTypePList),
        @"xml": @(ZCPDebugSandboxBrowseFileTypeXML),
        @"db": @(ZCPDebugSandboxBrowseFileTypeDatabase),
        @"js": @(ZCPDebugSandboxBrowseFileTypeJS),
        @"html": @(ZCPDebugSandboxBrowseFileTypeHTML),
        @"css": @(ZCPDebugSandboxBrowseFileTypeCSS),
        @"bin": @(ZCPDebugSandboxBrowseFileTypeBIN),
        @"dat": @(ZCPDebugSandboxBrowseFileTypeDat),
        @"sql": @(ZCPDebugSandboxBrowseFileTypeSQL),
        @"jar": @(ZCPDebugSandboxBrowseFileTypeJAR),
        // Adobe
        @"psd": @(ZCPDebugSandboxBrowseFileTypePSD),
        @"eps": @(ZCPDebugSandboxBrowseFileTypeEPS),
        // Other
        @"ttf": @(ZCPDebugSandboxBrowseFileTypeTTF),
        @"torrent": @(ZCPDebugSandboxBrowseFileTypeTorrent),
    };
    
    for (NSString *fileEx in map.allKeys) {
        if ([fileEx compare:fileEx options:NSCaseInsensitiveSearch] == NSOrderedSame) {
            type = [map[fileEx] integerValue];
        }
    }
    return type;
}

#pragma mark - getters

- (BOOL)isDirectory {
    return self.fileType == ZCPDebugSandboxBrowseFileTypeDirectory;
}

- (NSString *)typeImageName {
    if (!_typeImageName) {
        if (self.fileType == ZCPDebugSandboxBrowseFileTypeDirectory) {
            _typeImageName = (self.filesCount == 0) ? @"icon_file_type_folder_empty" : @"icon_file_type_folder_not_empty";
        } else {
            _typeImageName = @"icon_file_type_default";
        }
    }
    return _typeImageName;
}

- (UIImage *)typeImage {
    NSString *name = self.typeImageName;
    if (name.length == 0) {
        return nil;
    }
    
    NSURL *bundleURL = [[NSBundle bundleForClass:self.class] URLForResource:SandboxBrowseAssetsName withExtension:@"bundle"];
    if (!bundleURL) {
        return nil;
    }
    
    NSBundle *bundle = [NSBundle bundleWithURL:bundleURL];
    UIImage *image = [UIImage imageNamed:name inBundle:bundle compatibleWithTraitCollection:nil];
    return image;
}

- (BOOL)isCanPreviewInQuickLook {
    return [QLPreviewController canPreviewItem:self.URL];
}

- (BOOL)isCanPreviewInWebView {
    if (// Image
        self.fileType == ZCPDebugSandboxBrowseFileTypePNG ||
        self.fileType == ZCPDebugSandboxBrowseFileTypeJPG ||
        self.fileType == ZCPDebugSandboxBrowseFileTypeGIF ||
        self.fileType == ZCPDebugSandboxBrowseFileTypeSVG ||
        self.fileType == ZCPDebugSandboxBrowseFileTypeBMP ||
        // Audio
        self.fileType == ZCPDebugSandboxBrowseFileTypeWAV ||
        // Apple
        self.fileType == ZCPDebugSandboxBrowseFileTypeNumbers ||
        self.fileType == ZCPDebugSandboxBrowseFileTypePages ||
        self.fileType == ZCPDebugSandboxBrowseFileTypeKeynote ||
        // Microsoft
        self.fileType == ZCPDebugSandboxBrowseFileTypeWord ||
        self.fileType == ZCPDebugSandboxBrowseFileTypeExcel ||
        // Document
        self.fileType == ZCPDebugSandboxBrowseFileTypeTXT || // 编码问题
        self.fileType == ZCPDebugSandboxBrowseFileTypePDF ||
        self.fileType == ZCPDebugSandboxBrowseFileTypeMD ||
        // Programming
        self.fileType == ZCPDebugSandboxBrowseFileTypeJava ||
        self.fileType == ZCPDebugSandboxBrowseFileTypeSwift ||
        self.fileType == ZCPDebugSandboxBrowseFileTypeCSS ||
        // Adobe
        self.fileType == ZCPDebugSandboxBrowseFileTypePSD) {
        return YES;
    }
    
    return NO;
}

@end
