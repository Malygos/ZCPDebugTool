//
//  ZCPDebugSandboxBrowseFileInfoCell.m
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/8.
//

#import "ZCPDebugSandboxBrowseFileInfoCell.h"

@implementation ZCPDebugSandboxBrowseFileInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.accessoryType = UITableViewCellAccessoryNone;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        self.textLabel.textColor = [UIColor blackColor];
        self.textLabel.adjustsFontSizeToFitWidth = YES;
        
        self.detailTextLabel.textColor = [UIColor systemGrayColor];
        self.detailTextLabel.adjustsFontSizeToFitWidth = YES;
        self.detailTextLabel.font = [UIFont boldSystemFontOfSize:12];
        
        UIView *selectedView = [[UIView alloc] init];
        selectedView.backgroundColor = [UIColor clearColor];
        self.selectedBackgroundView = selectedView;
    }
    return self;
}

+ (NSString *)reuseIdentifier {
    return NSStringFromClass(self);
}

@end
