//
//  ZCPDebugSandboxBrowseVC.m
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/8.
//

#import "ZCPDebugSandboxBrowseVC.h"
#import "ZCPDebugSandboxBrowseFilePreviewVC.h"
#import "ZCPDebugSandboxBrowseFileInfoCell.h"
#import "ZCPDebugSandboxBrowseFileInfo.h"
#import <QuickLook/QuickLook.h>

@interface ZCPDebugSandboxBrowseVC () <UITableViewDataSource, UITableViewDelegate, QLPreviewControllerDataSource>

/// 文件/目录列表
@property (nonatomic, strong) UITableView *tableView;
/// 文件/目录信息数组
@property (nonatomic, strong) NSMutableArray *dataSource;
/// 正在使用QuickLook预览的文件
@property (nonatomic, strong) ZCPDebugSandboxBrowseFileInfo *previewingFileInfo;

@end

@implementation ZCPDebugSandboxBrowseVC

- (instancetype)init {
    if (self = [super init]) {
        _homeDirectory = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self loadDirectoryContents];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.tableView.frame = self.view.bounds;
}

#pragma mark - private

- (void)loadDirectoryContents {
    if (!self.fileInfo && self.isHomeDirectory) {
        NSURL *homeDirURL = [NSURL fileURLWithPath:NSHomeDirectory() isDirectory:YES];
        self.fileInfo = [[ZCPDebugSandboxBrowseFileInfo alloc] initWithFileURL:homeDirURL];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSMutableArray<ZCPDebugSandboxBrowseFileInfo *> *dataSource = [ZCPDebugSandboxBrowseFileInfo contentsOfDirectoryAtURL:self.fileInfo.URL];
        if (dataSource.count == 0) {
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            self.dataSource = dataSource;
            [self.tableView reloadData];
        });
    });
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCPDebugSandboxBrowseFileInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:[ZCPDebugSandboxBrowseFileInfoCell reuseIdentifier]];
    ZCPDebugSandboxBrowseFileInfo *fileInfo = self.dataSource[indexPath.row];
    cell.imageView.image = fileInfo.typeImage;
    cell.textLabel.text = fileInfo.displayName;
    cell.detailTextLabel.text = fileInfo.modificationDateText;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCPDebugSandboxBrowseFileInfo *fileInfo = self.dataSource[indexPath.row];
    
    if (fileInfo.isDirectory) {
        ZCPDebugSandboxBrowseVC *vc = [[ZCPDebugSandboxBrowseVC alloc] init];
        vc.fileInfo = fileInfo;
        vc.homeDirectory = NO;
        [self.navigationController pushViewController:vc animated:YES];
    } else if (fileInfo.isCanPreviewInQuickLook) {
        self.previewingFileInfo = fileInfo;
        QLPreviewController *vc = [[QLPreviewController alloc] init];
        vc.dataSource = self;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
//        if (fileInfo.URL) {
//            NSData *data = [NSData dataWithContentsOfURL:fileInfo.URL];
//            if (data) {
//                UIImage *image = [UIImage imageWithGIFData:data];
//                if (image) {
//                    _ImageController *vc = [[_ImageController alloc] initWithImage:image fileInfo:fileInfo];
//                    vc.hidesBottomBarWhenPushed = YES;//liman
//                    return vc;
//                }
//            }
//        }
        ZCPDebugSandboxBrowseFilePreviewVC *vc = [[ZCPDebugSandboxBrowseFilePreviewVC alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        vc.fileInfo = fileInfo;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - QLPreviewControllerDataSource

- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller {
    return self.previewingFileInfo ? 1 : 0;
}

- (id<QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index {
    return self.previewingFileInfo.URL;
}

#pragma mark - getters

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        _tableView.allowsMultipleSelectionDuringEditing = YES;
        _tableView.rowHeight = 60.0;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView registerClass:[ZCPDebugSandboxBrowseFileInfoCell class] forCellReuseIdentifier:[ZCPDebugSandboxBrowseFileInfoCell reuseIdentifier]];
    }
    return _tableView;
}

@end
