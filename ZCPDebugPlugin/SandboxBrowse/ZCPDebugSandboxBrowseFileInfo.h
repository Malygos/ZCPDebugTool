//
//  ZCPDebugSandboxBrowseFileInfo.h
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/8.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, ZCPDebugSandboxBrowseFileType) {
    ZCPDebugSandboxBrowseFileTypeUnknown,
    ZCPDebugSandboxBrowseFileTypeDirectory,
    // Image
    ZCPDebugSandboxBrowseFileTypeJPG,
    ZCPDebugSandboxBrowseFileTypePNG,
    ZCPDebugSandboxBrowseFileTypeGIF,
    ZCPDebugSandboxBrowseFileTypeSVG,
    ZCPDebugSandboxBrowseFileTypeBMP,
    ZCPDebugSandboxBrowseFileTypeTIF,
    // Audio
    ZCPDebugSandboxBrowseFileTypeMP3,
    ZCPDebugSandboxBrowseFileTypeAAC,
    ZCPDebugSandboxBrowseFileTypeWAV,
    ZCPDebugSandboxBrowseFileTypeOGG,
    // Video
    ZCPDebugSandboxBrowseFileTypeMP4,
    ZCPDebugSandboxBrowseFileTypeAVI,
    ZCPDebugSandboxBrowseFileTypeFLV,
    ZCPDebugSandboxBrowseFileTypeMIDI,
    ZCPDebugSandboxBrowseFileTypeMOV,
    ZCPDebugSandboxBrowseFileTypeMPG,
    ZCPDebugSandboxBrowseFileTypeWMV,
    // Apple
    ZCPDebugSandboxBrowseFileTypeDMG,
    ZCPDebugSandboxBrowseFileTypeIPA,
    ZCPDebugSandboxBrowseFileTypeNumbers,
    ZCPDebugSandboxBrowseFileTypePages,
    ZCPDebugSandboxBrowseFileTypeKeynote,
    // Google
    ZCPDebugSandboxBrowseFileTypeAPK,
    // Microsoft
    ZCPDebugSandboxBrowseFileTypeWord,
    ZCPDebugSandboxBrowseFileTypeExcel,
    ZCPDebugSandboxBrowseFileTypePPT,
    ZCPDebugSandboxBrowseFileTypeEXE,
    ZCPDebugSandboxBrowseFileTypeDLL,
    // Document
    ZCPDebugSandboxBrowseFileTypeTXT,
    ZCPDebugSandboxBrowseFileTypeRTF,
    ZCPDebugSandboxBrowseFileTypePDF,
    ZCPDebugSandboxBrowseFileTypeZIP,
    ZCPDebugSandboxBrowseFileType7z,
    ZCPDebugSandboxBrowseFileTypeCVS,
    ZCPDebugSandboxBrowseFileTypeMD,
    // Programming
    ZCPDebugSandboxBrowseFileTypeSwift,
    ZCPDebugSandboxBrowseFileTypeJava,
    ZCPDebugSandboxBrowseFileTypeC,
    ZCPDebugSandboxBrowseFileTypeCPP,
    ZCPDebugSandboxBrowseFileTypePHP,
    ZCPDebugSandboxBrowseFileTypeJSON,
    ZCPDebugSandboxBrowseFileTypePList,
    ZCPDebugSandboxBrowseFileTypeXML,
    ZCPDebugSandboxBrowseFileTypeDatabase,
    ZCPDebugSandboxBrowseFileTypeJS,
    ZCPDebugSandboxBrowseFileTypeHTML,
    ZCPDebugSandboxBrowseFileTypeCSS,
    ZCPDebugSandboxBrowseFileTypeBIN,
    ZCPDebugSandboxBrowseFileTypeDat,
    ZCPDebugSandboxBrowseFileTypeSQL,
    ZCPDebugSandboxBrowseFileTypeJAR,
    // Adobe
    ZCPDebugSandboxBrowseFileTypeFlash,
    ZCPDebugSandboxBrowseFileTypePSD,
    ZCPDebugSandboxBrowseFileTypeEPS,
    // Other
    ZCPDebugSandboxBrowseFileTypeTTF,
    ZCPDebugSandboxBrowseFileTypeTorrent,
};

@interface ZCPDebugSandboxBrowseFileInfo : NSObject

@property (nonatomic, strong) NSURL *URL;
@property (nonatomic, strong) NSString *displayName;
@property (nonatomic, strong) NSString *extension;
@property (nonatomic, strong) NSString *modificationDateText;
@property (nonatomic, strong) NSDictionary<NSString *, id> *attributes;
@property (nonatomic, assign, readonly) BOOL isDirectory;
@property (nonatomic, assign) NSUInteger filesCount;

@property (nonatomic, assign) ZCPDebugSandboxBrowseFileType fileType;

@property (nonatomic, strong, readonly) NSString *typeImageName;
@property (nonatomic, strong, readonly) UIImage *typeImage;
@property (nonatomic, assign, readonly) BOOL isCanPreviewInQuickLook;
@property (nonatomic, assign, readonly) BOOL isCanPreviewInWebView;

- (instancetype)initWithFileURL:(NSURL *)URL;

+ (NSDictionary<NSString *, id> *)attributesWithFileURL:(NSURL *)URL;
+ (NSMutableArray<ZCPDebugSandboxBrowseFileInfo *> *)contentsOfDirectoryAtURL:(NSURL *)URL;

@end

NS_ASSUME_NONNULL_END
