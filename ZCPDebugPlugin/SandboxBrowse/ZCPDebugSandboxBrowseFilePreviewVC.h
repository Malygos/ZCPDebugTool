//
//  ZCPDebugSandboxBrowseFilePreviewVC.h
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/8.
//

#import <UIKit/UIKit.h>
@class ZCPDebugSandboxBrowseFileInfo;

NS_ASSUME_NONNULL_BEGIN

@interface ZCPDebugSandboxBrowseFilePreviewVC : UIViewController

/// 沙盒文件信息
@property (nonatomic, strong) ZCPDebugSandboxBrowseFileInfo *fileInfo;

@end

NS_ASSUME_NONNULL_END
