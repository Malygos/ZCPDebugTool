//
//  ZCPDebugSandboxBrowsePlugin.m
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/7.
//

#import "ZCPDebugSandboxBrowsePlugin.h"
#import "ZCPDebugSandboxBrowseVC.h"
#import <ZCPDebugTool/ZCPDebugTool.h>

@implementation ZCPDebugSandboxBrowsePlugin

@synthesize pluginInfo = _pluginInfo;

+ (void)load {
    [ZCPDebugTool registerDebugPlugin:self];
}

- (ZCPDebugPluginInfo *)pluginInfo {
    if (!_pluginInfo) {
        _pluginInfo = [[ZCPDebugPluginInfo alloc] init];
        _pluginInfo.pluginId = NSStringFromClass(self.class);
        _pluginInfo.displayName = @"沙盒浏览";
        _pluginInfo.homePageClass = ZCPDebugSandboxBrowseVC.class;
    }
    return _pluginInfo;
}

@end
