//
//  ZCPDebugLogPlugin.h
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/9.
//

#import <Foundation/Foundation.h>
#import <ZCPDebugTool/ZCPDebugPluginBaseClass.h>

NS_ASSUME_NONNULL_BEGIN

FOUNDATION_EXPORT void ZCPAPMLog(NSString *format, ...);

@interface ZCPDebugLogPlugin : ZCPDebugPluginBaseClass

+ (void)hookNSLog;
+ (void)log:(NSString *)log;
- (void)log:(NSString *)log;

@end

NS_ASSUME_NONNULL_END
