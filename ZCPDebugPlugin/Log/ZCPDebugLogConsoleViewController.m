//
//  ZCPDebugLogConsoleViewController.m
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/9.
//

#import "ZCPDebugLogConsoleViewController.h"

@interface ZCPDebugLogConsoleViewController () <UITextViewDelegate> {
    BOOL _browsing;
    BOOL _dragging;
}
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UITextView *consoleTextView;
@property (nonatomic, strong) UIButton *moveToTopButton;
@property (nonatomic, strong) UIButton *moveToBottomButton;
@property (nonatomic, strong) UIButton *clearButton;

@end

@implementation ZCPDebugLogConsoleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hide)]];
    [self.view addSubview:self.containerView];
    [self.containerView addSubview:self.consoleTextView];
    [self.containerView addSubview:self.moveToTopButton];
    [self.containerView addSubview:self.moveToBottomButton];
    [self.containerView addSubview:self.clearButton];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    CGSize size = self.view.frame.size;
    CGFloat containerW = size.width - 32;
    CGFloat containerH = 450;
    CGFloat consoleH = containerH - 50;
    CGFloat buttonW = containerW / 3;
    CGFloat buttonTop = consoleH;
    
    self.containerView.frame = CGRectMake(16, (size.height - containerH) / 2, containerW, containerH);
    self.consoleTextView.frame = CGRectMake(0, 0, containerW, consoleH);
    self.moveToTopButton.frame = CGRectMake(0, consoleH, buttonW, 50);
    self.moveToBottomButton.frame = CGRectMake(buttonW, consoleH, buttonW, 50);
    self.clearButton.frame = CGRectMake(buttonW*2, consoleH, buttonW, 50);
}

#pragma mark - public method

- (void)addlog:(NSString *)log {
    if (self.consoleTextView.text.length == 0) {
        self.consoleTextView.text = log;
    } else {
        CGFloat maxLen = 10000;
        NSString *text = [NSString stringWithFormat:@"%@\n%@", self.consoleTextView.text, log];
        NSUInteger textLen = text.length;
        if (textLen > maxLen) {
            text = [text substringWithRange:NSMakeRange(textLen - maxLen, maxLen)];
        }
        self.consoleTextView.text = text;
    }
    
    if (!_dragging && !_browsing && self.consoleTextView.text.length > 0) {
        NSRange bottomRange = NSMakeRange(self.consoleTextView.text.length - 1, 1);
        [self.consoleTextView scrollRangeToVisible:bottomRange];
    }
}

- (void)show {
    self.view.window.hidden = NO;
}

- (void)hide {
    self.view.window.hidden = YES;
    if (self.hideAction) {
        self.hideAction();
    }
}

- (void)moveToTopAction {
    NSRange topRange = NSMakeRange(0, 1);
    [self.consoleTextView scrollRangeToVisible:topRange];
}

- (void)moveToBottomAction {
    NSRange bottomRange = NSMakeRange(self.consoleTextView.text.length - 1, 1);
    [self.consoleTextView scrollRangeToVisible:bottomRange];
}

- (void)clearAction {
    self.consoleTextView.text = @"";
}

#pragma mark - UITextViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    _dragging = YES;
    _browsing = YES;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    _dragging = NO;
    if (scrollView.contentOffset.y + scrollView.frame.size.height < scrollView.contentSize.height) {
        _browsing = YES;
    } else {
        _browsing = NO;
    }
}

#pragma mark - getters

- (UIView *)containerView {
    if (!_containerView) {
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor whiteColor];
        _containerView.layer.cornerRadius = 8;
        _containerView.layer.masksToBounds = YES;
    }
    return _containerView;
}

- (UITextView *)consoleTextView {
    if (!_consoleTextView) {
        _consoleTextView = [[UITextView alloc] init];
        _consoleTextView.backgroundColor = [UIColor whiteColor];
        _consoleTextView.font = [UIFont systemFontOfSize:10];
        _consoleTextView.textColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1];
        _consoleTextView.editable = NO;
        _consoleTextView.delegate = self;
    }
    return _consoleTextView;
}

- (UIButton *)moveToTopButton {
    if (!_moveToTopButton) {
        _moveToTopButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _moveToTopButton.backgroundColor = [UIColor lightGrayColor];
        [_moveToTopButton setTitle:@"回到顶部" forState:UIControlStateNormal];
        [_moveToTopButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_moveToTopButton addTarget:self action:@selector(moveToTopAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moveToTopButton;
}

- (UIButton *)moveToBottomButton {
    if (!_moveToBottomButton) {
        _moveToBottomButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _moveToBottomButton.backgroundColor = [UIColor lightGrayColor];
        [_moveToBottomButton setTitle:@"回到底部" forState:UIControlStateNormal];
        [_moveToBottomButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_moveToBottomButton addTarget:self action:@selector(moveToBottomAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moveToBottomButton;
}

- (UIButton *)clearButton {
    if (!_clearButton) {
        _clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _clearButton.backgroundColor = [UIColor redColor];
        [_clearButton setTitle:@"清空" forState:UIControlStateNormal];
        [_clearButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_clearButton addTarget:self action:@selector(clearAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _clearButton;
}

@end
