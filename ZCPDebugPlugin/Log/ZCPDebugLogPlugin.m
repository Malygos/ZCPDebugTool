//
//  ZCPDebugLogPlugin.m
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/9.
//

#import "ZCPDebugLogPlugin.h"
#import "ZCPDebugLogConsoleViewController.h"
#import <ZCPDebugTool/ZCPDebugTool.h>
#import <fishhook/fishhook.h>

#define ZCPDebugLogPluginCacheKey   @"ZCPDebugLogPluginCacheKey"
#define ZCPDebugCacheFloder         @"com.zcp.debug"
#define ZCPDebugCrashReportFloder   @"Log"

static ZCPDebugLogPlugin *_zcpapm_logPlugin;

void ZCPAPMLog(NSString *format, ...) {
    va_list args;
    va_start(args, format);
    NSString *log = [[NSString alloc] initWithFormat:format arguments:args];
    va_end(args);
    
    [ZCPDebugLogPlugin log:log];
}

static void (*_zcpapm_origin_nslog)(NSString *format, ...);
void _zcpapm_new_nslog(NSString *format, ...) {
    if (![format isKindOfClass:[NSString class]]) {
        return;
    }
    va_list vl;
    va_start(vl, format);
    NSString *str = [[NSString alloc] initWithFormat:format arguments:vl];
    _zcpapm_origin_nslog(str);
    [ZCPDebugLogPlugin log:str];
    va_end(vl);
}


@interface ZCPDebugLogPlugin ()

@property (nonatomic, copy) NSString *logCacheDir;
@property (nonatomic, copy) NSString *logFilePath;
@property (nonatomic, strong) NSFileHandle *fileHandle;

@property (nonatomic, strong) UIWindow *consoleLogWindow;
@property (nonatomic, strong) ZCPDebugLogConsoleViewController *consoleLogVC;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) dispatch_queue_t serialQueue;

@end

@implementation ZCPDebugLogPlugin

@synthesize pluginInfo = _pluginInfo;

+ (void)load {
    [ZCPDebugTool registerDebugPlugin:self];
}

#pragma mark - ZCPAPMPluginProtocol

- (ZCPDebugPluginInfo *)pluginInfo {
    if (!_pluginInfo) {
        NSDictionary *cachedInfo = [[NSUserDefaults standardUserDefaults] objectForKey:ZCPDebugLogPluginCacheKey];
        
        _pluginInfo = [[ZCPDebugPluginInfo alloc] init];
        _pluginInfo.pluginId = NSStringFromClass(self.class);
        _pluginInfo.displayName = @"日志";
        
        _pluginInfo.hasSwitch = YES;
        _pluginInfo.on = cachedInfo[SELToStr(on)];
        _pluginInfo.needRestartAppWhenOpen = NO;
        _pluginInfo.needRestartAppWhenClose = YES;
        AddKVO(_pluginInfo, self, on);
    }
    return _pluginInfo;
}

- (BOOL)firstLaunch {
    [ZCPDebugLogPlugin hookNSLog];
    _zcpapm_logPlugin = self;
    return YES;
}

- (BOOL)launch {
    _zcpapm_logPlugin = self;
    return YES;
}

- (BOOL)finish {
    _zcpapm_logPlugin = nil;
    return YES;
}

- (void)quickAccessWithCompletion:(dispatch_block_t)closeBlock {
    self.consoleLogWindow.hidden = NO;
    self.consoleLogVC.hideAction = closeBlock;
}

#pragma mark - pluginInfo observer

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if ([keyPath isEqual:SELToStr(on)]) {
        BOOL new = [KVONew(change) boolValue];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSMutableDictionary *info = [[userDefaults objectForKey:ZCPDebugLogPluginCacheKey] mutableCopy];
        if (!info) {
            info = [NSMutableDictionary dictionary];
        }
        info[SELToStr(on)] = @(new);
        [userDefaults setObject:info forKey:ZCPDebugLogPluginCacheKey];
        [userDefaults synchronize];
        
        [self handleSwitch:new];
    }
}

#pragma mark - public

+ (void)hookNSLog {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        struct rebinding nslog_rebinding = {"NSLog", _zcpapm_new_nslog, (void *)&_zcpapm_origin_nslog};
        rebind_symbols((struct rebinding[1]){nslog_rebinding}, 1);
    });
}

+ (void)log:(NSString *)log {
    [_zcpapm_logPlugin log:log];
}

- (void)log:(NSString *)log {
    if (!log) {
        return;
    }
    
    self.dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss.SSS";
    NSString *time = [self.dateFormatter stringFromDate:[NSDate date]];
    log = [NSString stringWithFormat:@"%@ %@", time, log];
    
    [self writeLogToFile:log];
    [self showLogInDebugView:log];
}

#pragma mark - private

- (void)writeLogToFile:(NSString *)log {
    if (!log || ![log isKindOfClass:NSString.class]) return;
    log = [NSString stringWithFormat:@"\n%@", log];
    dispatch_async(self.serialQueue, ^{
        NSData *buffer = [log dataUsingEncoding:NSUTF8StringEncoding];
        if (!buffer) return;
        
        if (!self.fileHandle) {
            self.fileHandle = [NSFileHandle fileHandleForWritingAtPath:self.logFilePath];
        }
        if (!self.fileHandle) return;
        
        [self.fileHandle seekToEndOfFile];
        [self.fileHandle writeData:buffer];
        [self.fileHandle synchronizeFile];
//        [self.fileHandle closeFile];
    });
}

- (void)showLogInDebugView:(NSString *)log {
    dispatch_block_t block = ^{
        [self.consoleLogVC addlog:log];
    };
    if ([NSThread isMainThread]) {
        block();
    } else {
        dispatch_async(dispatch_get_main_queue(), block);
    }
}

#pragma mark - getters

- (NSString *)logFilePath {
    if (!_logFilePath) {
        self.dateFormatter.dateFormat = @"yyyy-MM-dd_HH.mm.ss.SSS";
        NSString *time = [self.dateFormatter stringFromDate:[NSDate date]];
        NSString *fileName = [NSString stringWithFormat:@"%@.log", time];
        NSString *filePath = [NSString stringWithFormat:@"%@/%@", self.logCacheDir, fileName];
        if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            [[NSFileManager defaultManager] createFileAtPath:filePath contents:nil attributes:nil];
        }
        _logFilePath = filePath;
    }
    return _logFilePath;
}

- (NSString *)logCacheDir {
    if (!_logCacheDir) {
        NSString *dir = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];;
        dir = [dir stringByAppendingFormat:@"/%@/%@", ZCPDebugCacheFloder, ZCPDebugCrashReportFloder];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:dir]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:dir withIntermediateDirectories:YES attributes:nil error:nil];
        }
        _logCacheDir = dir;
    }
    return _logCacheDir;
}

- (UIWindow *)consoleLogWindow {
    if (!_consoleLogWindow) {
        _consoleLogWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _consoleLogWindow.hidden = YES;
        _consoleLogWindow.rootViewController = self.consoleLogVC;
    }
    return _consoleLogWindow;
}

- (ZCPDebugLogConsoleViewController *)consoleLogVC {
    if (!_consoleLogVC) {
        _consoleLogVC = [[ZCPDebugLogConsoleViewController alloc] init];
    }
    return _consoleLogVC;
}

- (NSDateFormatter *)dateFormatter {
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
    }
    return _dateFormatter;
}

- (dispatch_queue_t)serialQueue {
    if (!_serialQueue) {
        _serialQueue = dispatch_queue_create("com.zcp.debug.log.queue", DISPATCH_QUEUE_SERIAL);
    }
    return _serialQueue;
}

@end
