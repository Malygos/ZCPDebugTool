//
//  ZCPDebugLogConsoleViewController.h
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/9.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZCPDebugLogConsoleViewController : UIViewController

@property (nonatomic, copy) dispatch_block_t hideAction;

- (void)addlog:(NSString *)log;

@end

NS_ASSUME_NONNULL_END
