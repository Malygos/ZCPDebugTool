//
//  ZCPDebugCrashReportPlugin.m
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/7.
//

#import "ZCPDebugCrashReportPlugin.h"
#import <ZCPDebugTool/ZCPDebugTool.h>

#define ZCPDebugCacheFloder       @"com.zcp.debug"
#define ZCPDebugCrashReportFloder @"CrashReport"
#define ZCPDebugCrashReportPluginCacheKey @"CrashReport"

@interface ZCPDebugCrashReportPlugin ()

@property (nonatomic, assign) BOOL needHandle;
@property (nonatomic, copy) NSString *crashReportLogDir;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;

- (void)handleException:(NSException *)exception;

@end

static ZCPDebugCrashReportPlugin *_zcpapm_crashReportPlugin;
static NSUncaughtExceptionHandler *_zcpapm_preUncaughtExceptionHandler = nil;
static void ZCPAPMUncaughtExceptionHandler(NSException *exception) {
    if (_zcpapm_preUncaughtExceptionHandler) {
        _zcpapm_preUncaughtExceptionHandler(exception);
    }
    [_zcpapm_crashReportPlugin handleException:exception];
}

@implementation ZCPDebugCrashReportPlugin

@synthesize status = _status;
@synthesize pluginInfo = _pluginInfo;

+ (void)load {
    [ZCPDebugTool registerDebugPlugin:self];
}

#pragma mark - ZCPAPMPluginProtocol

- (ZCPDebugPluginInfo *)pluginInfo {
    if (!_pluginInfo) {
        NSDictionary *cachedInfo = [[NSUserDefaults standardUserDefaults] objectForKey:ZCPDebugCrashReportPluginCacheKey];
        
        _pluginInfo = [[ZCPDebugPluginInfo alloc] init];
        _pluginInfo.pluginId = NSStringFromClass(self.class);
        _pluginInfo.displayName = @"Crash监听";
        _pluginInfo.homePageClass = nil;
        
        _pluginInfo.hasSwitch = YES;
        _pluginInfo.on = cachedInfo[SELToStr(on)];
        _pluginInfo.needRestartAppWhenOpen = NO;
        _pluginInfo.needRestartAppWhenClose = YES;
        AddKVO(_pluginInfo, self, on);
    }
    return _pluginInfo;
}

- (BOOL)firstLaunch {
    _zcpapm_crashReportPlugin = self;
    _zcpapm_preUncaughtExceptionHandler = NSGetUncaughtExceptionHandler();
    NSSetUncaughtExceptionHandler(ZCPAPMUncaughtExceptionHandler);
    _needHandle = YES;
    return YES;
}

- (BOOL)launch {
    _needHandle = YES;
    return YES;
}

- (BOOL)finish {
    _needHandle = NO;
    return YES;
}

#pragma mark - pluginInfo observer

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if ([keyPath isEqual:SELToStr(on)]) {
        BOOL new = [KVONew(change) boolValue];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSMutableDictionary *info = [[userDefaults objectForKey:ZCPDebugCrashReportPluginCacheKey] mutableCopy];
        if (!info) {
            info = [NSMutableDictionary dictionary];
        }
        info[SELToStr(on)] = @(new);
        [userDefaults setObject:info forKey:ZCPDebugCrashReportPluginCacheKey];
        [userDefaults synchronize];
        
        [self handleSwitch:new];
    }
}

#pragma mark - private

- (void)handleException:(NSException *)exception {
    if (!_needHandle) {
        return;
    }
    
    NSString *logName = [NSString stringWithFormat:@"%@.log", self.nowDateString];
    NSString *fileName = [NSString stringWithFormat:@"%@/%@", self.crashReportLogDir, logName];
    
    NSString *reason = exception.reason;
    NSDictionary *userInfo = exception.userInfo;
    NSArray *callStackSymbols = exception.callStackSymbols;
    NSArray *callStackReturnAddresses = exception.callStackReturnAddresses;
    NSDictionary *logInfo = @{
        @"reason": reason?:@"",
        @"userInfo": userInfo?:@"",
        @"callStackSymbols": callStackSymbols?:@"",
        @"callStackReturnAddresses": callStackReturnAddresses?:@""
    };
    NSString *log = logInfo.description;
    [log writeToFile:fileName atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

#pragma mark - utils

- (NSString *)nowDateString {
    return [self.dateFormatter stringFromDate:[NSDate date]];
}

#pragma mark - getters

- (NSString *)crashReportLogDir {
    if (!_crashReportLogDir) {
        NSString *logDir = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];;
        logDir = [logDir stringByAppendingFormat:@"/%@/%@", ZCPDebugCacheFloder, ZCPDebugCrashReportFloder];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:logDir]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:logDir withIntermediateDirectories:YES attributes:nil error:nil];
        }
        _crashReportLogDir = logDir;
    }
    return _crashReportLogDir;
}

- (NSDateFormatter *)dateFormatter {
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = @"YY-MM-dd_HH.mm.ss.SSS";
    }
    return _dateFormatter;
}

@end
