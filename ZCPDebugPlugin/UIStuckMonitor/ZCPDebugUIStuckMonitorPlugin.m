//
//  ZCPDebugUIStuckMonitorPlugin.m
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/7.
//

#import "ZCPDebugUIStuckMonitorPlugin.h"
#import <ZCPDebugTool/ZCPDebugTool.h>

@implementation ZCPDebugUIStuckMonitorPlugin

@synthesize pluginInfo = _pluginInfo;

#pragma mark - ZCPAPMPluginProtocol

+ (void)load {
    [ZCPDebugTool registerDebugPlugin:self];
}

- (ZCPDebugPluginInfo *)pluginInfo {
    if (!_pluginInfo) {
        _pluginInfo = [[ZCPDebugPluginInfo alloc] init];
        _pluginInfo.pluginId = NSStringFromClass(self.class);
        _pluginInfo.displayName = @"UI卡顿监测（不可用）";
        _pluginInfo.homePageClass = nil;
    }
    return _pluginInfo;
}

@end
