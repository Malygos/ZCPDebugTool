//
//  ZCPDebugUIStuckMonitorPlugin.h
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/7.
//

#import <Foundation/Foundation.h>
#import <ZCPDebugTool/ZCPDebugPluginBaseClass.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZCPDebugUIStuckMonitorPlugin : ZCPDebugPluginBaseClass

@end

NS_ASSUME_NONNULL_END
