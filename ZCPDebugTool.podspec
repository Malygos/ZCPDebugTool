Pod::Spec.new do |s|

  s.name          = "ZCPDebugTool"
  s.version       = "1.0.0"
  s.author        = { "朱超鹏" => "z164757979@163.com" }
  s.license       = { :type => 'MIT', :file => 'LICENSE' }
  s.homepage      = "https://gitlab.com/Malygos/ZCPDebugTool"
  s.source        = { :git => "https://gitlab.com/Malygos/ZCPDebugTool.git", :tag => "#{s.version}" }
  s.summary       = "ZCPDebugTool."
  s.description   = <<-DESC
                       It`s a ZCPDebugTool.
                       DESC

  s.platform      = :ios, '9.0'
  s.ios.deployment_target = '9.0'
  s.module_name   = 'ZCPDebugTool'
  s.framework     = 'Foundation', 'UIKit'

  # source_files start
  s.source_files = 'ZCPDebugTool/ZCPDebugTool.{h,m}'
  
  s.subspec 'UI' do |ss|
    ss.source_files = 'ZCPDebugTool/UI/**/*.{h,m}'
  end
  
  s.subspec 'PluginProtocol' do |ss|
    ss.source_files = 'ZCPDebugTool/PluginProtocol/**/*.{h,m}'
  end
  # source_files end

end
