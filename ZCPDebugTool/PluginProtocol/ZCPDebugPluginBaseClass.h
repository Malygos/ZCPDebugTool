//
//  ZCPDebugPluginBaseClass.h
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/7.
//

#import <Foundation/Foundation.h>
#import <ZCPDebugTool/ZCPDebugPluginInfo.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, ZCPDebugToolPluginStatus) {
    ZCPDebugToolPluginStatusInitial,
    ZCPDebugToolPluginStatusLaunching,
    ZCPDebugToolPluginStatusRunning,
    ZCPDebugToolPluginStatusFinishing,
    ZCPDebugToolPluginStatusFinished
};

@protocol ZCPDebugToolPluginProtocol <NSObject>

@property (nonatomic, strong) ZCPDebugPluginInfo *pluginInfo;
@property (nonatomic, assign) ZCPDebugToolPluginStatus status;

- (BOOL)install;
- (BOOL)uninstall;
- (void)handleSwitch:(BOOL)isOn;
- (void)quickAccessWithCompletion:(dispatch_block_t)closeBlock;

// need subclass override
- (BOOL)firstLaunch;
- (BOOL)launch;
- (BOOL)finish;

@end

@interface ZCPDebugPluginBaseClass : NSObject <ZCPDebugToolPluginProtocol>
@end

NS_ASSUME_NONNULL_END
