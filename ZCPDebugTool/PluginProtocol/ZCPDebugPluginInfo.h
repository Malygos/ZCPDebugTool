//
//  ZCPDebugPluginInfo.h
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/13.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define SELToStr(property) NSStringFromSelector(@selector(property))
#define AddKVO(target, observer, property) [target addObserver:observer forKeyPath:SELToStr(property) options:NSKeyValueObservingOptionNew context:nil]
#define KVONew(change) change[NSKeyValueChangeNewKey]

@interface ZCPDebugPluginInfo : NSObject

/// 插件唯一编号
@property (nonatomic, copy) NSString *pluginId;
/// 插件显示名称
@property (nonatomic, copy) NSString *displayName;
/// 插件自己的首页Class
@property (nonatomic, strong, nullable) Class homePageClass;
/// 插件自己的设置页Class
@property (nonatomic, strong, nullable) Class settingPageClass;

/// 是否有开关，默认为NO
@property (nonatomic, assign) BOOL hasSwitch;
/// 是否打开，默认为NO
@property (nonatomic, assign, getter=isOn) BOOL on;
/// 打开时是否需要重启App，默认为NO
@property (nonatomic, assign) BOOL needRestartAppWhenOpen;
/// 关闭时是否需要重启App，默认为NO
@property (nonatomic, assign) BOOL needRestartAppWhenClose;

@end

NS_ASSUME_NONNULL_END
