//
//  ZCPDebugPluginBaseClass.m
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/7.
//

#import "ZCPDebugPluginBaseClass.h"

@implementation ZCPDebugPluginBaseClass

@synthesize pluginInfo = _pluginInfo;
@synthesize status = _status;

- (BOOL)install {
    ZCPDebugToolPluginStatus oldStatus = self.status;
    if (oldStatus != ZCPDebugToolPluginStatusInitial &&
        oldStatus != ZCPDebugToolPluginStatusFinished) {
        return NO;
    }
    
    self.status = ZCPDebugToolPluginStatusLaunching;
    
    BOOL result = NO;
    if (oldStatus == ZCPDebugToolPluginStatusInitial) {
        result = [self firstLaunch];
    } else if (oldStatus == ZCPDebugToolPluginStatusFinished) {
        result = [self launch];
    }
    
    if (result) {
        self.status = ZCPDebugToolPluginStatusRunning;
    } else {
        self.status = oldStatus;
    }
    
    return result;
}

- (BOOL)uninstall {
    if (self.status != ZCPDebugToolPluginStatusRunning) {
        return NO;
    }
    
    self.status = ZCPDebugToolPluginStatusFinishing;
    [self finish];
    self.status = ZCPDebugToolPluginStatusFinished;
    return YES;
}

- (BOOL)firstLaunch {
    return [self launch];
}

- (BOOL)launch {
    return YES;
}

- (BOOL)finish {
    return YES;
}

- (void)handleSwitch:(BOOL)isOn {
    if (isOn) {
        [self install];
    } else {
        [self uninstall];
    }
}

@end
