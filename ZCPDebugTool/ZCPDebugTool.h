//
//  ZCPDebugTool.h
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/7.
//

#import <Foundation/Foundation.h>
#import <ZCPDebugTool/ZCPDebugPluginInfo.h>
#import <ZCPDebugTool/ZCPDebugPluginBaseClass.h>

/*
 APM：应用性能管理（Application Performance Management）
 
 后续的想法：
 写的这些插件其实是debug工具不能算是apm插件，可以分成两个工具。
 apm：fps，ui卡顿监控，内存，网络状态，...
 debug：日志，沙盒，路由，...
 
 
 对于apm分成两个库，一个APM库一个plugin库，APM库对外提供plugin协议，每个apm插件需要按照协议去实现。
 对于debug库，同上拆分，但是想做成任意二方库都可以自己往里插入插件的形式。比如跑步模块需要定制一个实时查看定位点的debug工具，就可以在自己的模块内实现插件插入debug库中。另外需要考虑的是Release环境下如何轻松剥离debug工具
 不知道podsepc能否根据debug和release来控制subspec的引入
*/


NS_ASSUME_NONNULL_BEGIN

@interface ZCPDebugTool : NSObject

+ (instancetype)shared;
+ (void)launch;
+ (void)registerDebugPlugin:(Class<ZCPDebugToolPluginProtocol>)pluginClass;

@end

NS_ASSUME_NONNULL_END
