//
//  ZCPDebugTool.m
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/7.
//

#import "ZCPDebugTool.h"
#import "ZCPDebugToolFloatBallVC.h"
#import "ZCPDebugToolPlulginListVC.h"
#import "ZCPDebugPluginBaseClass.h"
#import <objc/runtime.h>
#import <pthread/pthread.h>

#define ZCPDebugToolQuickUsePluginKey @"ZCPDebugToolQuickUsePluginKey"
#define LOCK    pthread_mutex_lock(&[ZCPDebugTool shared]->_lock)
#define UNLOCK  pthread_mutex_unlock(&[ZCPDebugTool shared]->_lock)

@interface ZCPDebugTool () {
    pthread_mutex_t _lock;
}

/// 悬浮球window
@property (nonatomic, strong) UIWindow *floatBallWindow;
/// 插件列表window
@property (nonatomic, strong) UIWindow *pluginListWindow;

/// 特权插件
@property (nonatomic, strong) id<ZCPDebugToolPluginProtocol> quickAccessPlugin;
@property (nonatomic, assign) NSUInteger quickAccessPluginIndex;

/// 插件信息列表
@property (nonatomic, strong) NSMutableArray *pluginClasses;
/// 插件实例列表
@property (nonatomic, copy) NSArray *plugins;
/// 插件ID : 插件实例
@property (nonatomic, copy) NSDictionary *pluginMap;
/// 插件是否已启动
@property (nonatomic, assign, getter=isLaunching) BOOL launching;

@end

@implementation ZCPDebugTool

#pragma mark - init

+ (instancetype)shared {
    static id __instance__;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __instance__ = [[self alloc] init];
    });
    return __instance__;
}

- (instancetype)init {
    if (self = [super init]) {
        pthread_mutex_init(&_lock, NULL);
    }
    return self;
}

#pragma mark - public

+ (void)launch {
    LOCK;
    
    ZCPDebugTool *debugTool = [ZCPDebugTool shared];
    if (debugTool.launching) {
        UNLOCK;
        return;
    }
    debugTool.launching = YES;
    
    // 读取快捷使用插件
    NSString *quickAccessPluginId = [self getQuickUsePluginId];
    debugTool.quickAccessPlugin = nil;
    debugTool.quickAccessPluginIndex = NSUIntegerMax;
    
    NSMutableArray *plugins = [NSMutableArray array];
    NSMutableDictionary *pluginMap = [NSMutableDictionary dictionary];
    
    // 创建插件
    for (int i = 0; i < debugTool.pluginClasses.count; i++) {
        Class pluginClass = debugTool.pluginClasses[i];
        id<ZCPDebugToolPluginProtocol> pluginObj = [[pluginClass alloc] init];
        ZCPDebugPluginInfo *pluginInfo = pluginObj.pluginInfo;
        NSString *pluginId = pluginInfo.pluginId;
        
        // 安装插件
        if (!pluginInfo.hasSwitch) {
            [pluginObj install];
        } else if (pluginInfo.isOn) {
            [pluginObj install];
        }
        
        // 记录快捷使用插件
        if ([pluginId isEqualToString:quickAccessPluginId]) {
            debugTool.quickAccessPlugin = pluginObj;
            debugTool.quickAccessPluginIndex = i;
        }
        
        [plugins addObject:pluginObj];
        [pluginMap setObject:pluginObj forKey:pluginId];
    }
    
    debugTool.plugins = plugins;
    debugTool.pluginMap = pluginMap;
    debugTool.floatBallWindow.hidden = NO;
    debugTool.pluginListWindow.hidden = YES;
    
    NSLog(@"[ZCPDebugTool] 已识别：%li个插件", debugTool.plugins.count);
    
    UNLOCK;
}

+ (void)registerDebugPlugin:(Class<ZCPDebugToolPluginProtocol>)pluginClass {
    LOCK;
    
    if (![pluginClass conformsToProtocol:@protocol(ZCPDebugToolPluginProtocol)]) {
        UNLOCK;
        return;
    }
    if ([ZCPDebugTool shared].isLaunching) {
        UNLOCK;
        return;
    }
    [[ZCPDebugTool shared].pluginClasses addObject:pluginClass];
    
    UNLOCK;
}

#pragma mark - quick use plugin

+ (NSString *)getQuickUsePluginId {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *quickAccessPluginId = [userDefaults objectForKey:ZCPDebugToolQuickUsePluginKey];
    return quickAccessPluginId;
}

#pragma mark - getters

- (UIWindow *)floatBallWindow {
    if (!_floatBallWindow) {
        _floatBallWindow = [[UIWindow alloc] init];
        _floatBallWindow.frame = [ZCPDebugToolFloatBallVC defaultBallRect];
        _floatBallWindow.backgroundColor = [UIColor clearColor];
        
        ZCPDebugToolFloatBallVC *vc = [[ZCPDebugToolFloatBallVC alloc] init];
        _floatBallWindow.rootViewController = vc;
        
        __weak typeof(self) weak_self = self;
        vc.singleClickAction = ^{
            if ([weak_self.quickAccessPlugin respondsToSelector:@selector(quickAccessWithCompletion:)]) {
                [weak_self.quickAccessPlugin quickAccessWithCompletion:^{
                    weak_self.floatBallWindow.hidden = NO;
                }];
            } else {
                weak_self.pluginListWindow.hidden = NO;
            }
        };
        vc.longPressAction = ^{
            weak_self.pluginListWindow.hidden = NO;
        };
    }
    return _floatBallWindow;
}

- (UIWindow *)pluginListWindow {
    if (!_pluginListWindow) {
        _pluginListWindow = [[UIWindow alloc] init];
        _pluginListWindow.frame = [UIScreen mainScreen].bounds;
        _pluginListWindow.backgroundColor = [UIColor whiteColor];
        
        ZCPDebugToolPlulginListVC *vc = [[ZCPDebugToolPlulginListVC alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        _pluginListWindow.rootViewController = nav;
        
        NSMutableArray *pluginInfos = [NSMutableArray array];
        for (id<ZCPDebugToolPluginProtocol> plugin in self.plugins) {
            ZCPDebugPluginInfo *pluginInfo = plugin.pluginInfo;
            if (pluginInfo) {
                [pluginInfos addObject:plugin.pluginInfo];
            }
        }
        vc.pluginInfos = pluginInfos;
        
        __weak typeof(self) weak_self = self;
        vc.hideAction = ^{
            weak_self.floatBallWindow.hidden = NO;
        };
        vc.quickAccessPluginSelectedIndex = self.quickAccessPluginIndex;
        vc.quickAccessPluginSelectedAction = ^(NSUInteger index) {
            self.quickAccessPlugin = self.plugins[index];
            NSString *pluginId = self.quickAccessPlugin.pluginInfo.pluginId;
            [[NSUserDefaults standardUserDefaults] setObject:pluginId forKey:ZCPDebugToolQuickUsePluginKey];
        };
    }
    return _pluginListWindow;
}

- (NSMutableArray *)pluginClasses {
    if (!_pluginClasses) {
        _pluginClasses = [[NSMutableArray alloc] init];
    }
    return _pluginClasses;
}

@end
