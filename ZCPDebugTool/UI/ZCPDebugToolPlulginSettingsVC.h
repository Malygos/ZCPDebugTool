//
//  ZCPDebugToolPlulginSettingsVC.h
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/13.
//

#import <UIKit/UIKit.h>
#import <ZCPDebugTool/ZCPDebugPluginBaseClass.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZCPDebugToolPlulginSettingsVC : UIViewController

@property (nonatomic, strong) ZCPDebugPluginInfo *pluginInfo;
@property (nonatomic, copy) dispatch_block_t updateAction;

@end

NS_ASSUME_NONNULL_END
