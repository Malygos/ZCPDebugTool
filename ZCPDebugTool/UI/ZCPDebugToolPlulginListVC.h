//
//  ZCPDebugToolPlulginListVC.h
//  Pods
//
//  Created by 朱超鹏 on 2021/3/7.
//

#import <UIKit/UIKit.h>
#import <ZCPDebugTool/ZCPDebugPluginInfo.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^ZCPDebugToolQuickUsePluginSelectedAction)(NSUInteger index);

@interface ZCPDebugToolPlulginListVC : UIViewController

@property (nonatomic, copy) dispatch_block_t hideAction;
@property (nonatomic, assign) NSUInteger quickAccessPluginSelectedIndex;
@property (nonatomic, copy) ZCPDebugToolQuickUsePluginSelectedAction quickAccessPluginSelectedAction;
@property (nonatomic, copy) NSArray <ZCPDebugPluginInfo *>*pluginInfos;

@end

NS_ASSUME_NONNULL_END
