//
//  ZCPDebugToolPlulginListVC.m
//  Pods
//
//  Created by 朱超鹏 on 2021/3/7.
//

#import "ZCPDebugToolPlulginListVC.h"
#import "ZCPDebugToolPlulginSettingsVC.h"

@interface _ZCPDebugToolPlulginListCell : UICollectionViewCell
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, copy) dispatch_block_t longPressAction;
@end
@implementation _ZCPDebugToolPlulginListCell
+ (NSString *)reuseIdentifier {
    return NSStringFromClass(self);
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self.contentView addSubview:self.titleLabel];
        [self addGestureRecognizer:[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)]];
    }
    return self;
}
- (void)layoutSubviews {
    [super layoutSubviews];
    self.titleLabel.frame = self.bounds;
}
- (void)longPress:(UILongPressGestureRecognizer *)longPress {
    if (longPress.state == UIGestureRecognizerStateBegan) {
        if (self.longPressAction) {
            self.longPressAction();
        }
//        [NSObject cancelPreviousPerformRequestsWithTarget:self];
    }
}
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.numberOfLines = 0;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [UIFont systemFontOfSize:15];
    }
    return _titleLabel;
}
@end


@interface ZCPDebugToolPlulginListVC () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) UICollectionView *pluginListView;

@end

@implementation ZCPDebugToolPlulginListVC

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(back)];
    
    [self.view addSubview:self.pluginListView];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.pluginListView.frame = self.view.bounds;
}

#pragma mark - event response

- (void)back {
    self.view.window.hidden = YES;
    if (self.hideAction) self.hideAction();
}

#pragma mark - UICollectionViewDataSource, UICollectionViewDelegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.pluginInfos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    _ZCPDebugToolPlulginListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[_ZCPDebugToolPlulginListCell reuseIdentifier] forIndexPath:indexPath];
    ZCPDebugPluginInfo *pluginInfo = self.pluginInfos[indexPath.row];
    cell.titleLabel.text = pluginInfo.displayName;
    
    if (!pluginInfo.hasSwitch || (pluginInfo.hasSwitch && pluginInfo.isOn)) {
        if (self.quickAccessPluginSelectedIndex == indexPath.row) {
            cell.backgroundColor = [UIColor cyanColor];
        } else {
            cell.backgroundColor = [UIColor greenColor];
        }
        cell.longPressAction = ^{
            if (self.quickAccessPluginSelectedAction) {
                self.quickAccessPluginSelectedAction(indexPath.row);
            }
            self.quickAccessPluginSelectedIndex = indexPath.row;
            [self.pluginListView reloadData];
        };
    } else {
        cell.backgroundColor = [UIColor redColor];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ZCPDebugPluginInfo *pluginInfo = self.pluginInfos[indexPath.row];
    Class pageClass = nil;
    UIViewController *page = nil;
    
    // 若插件有自己的首页，则跳转，若没有首页则判断是否有设置页，有则跳转
    if (pluginInfo.homePageClass) {
        pageClass = pluginInfo.homePageClass;
    } else if (pluginInfo.settingPageClass) {
        pageClass = pluginInfo.settingPageClass;
    }
    
    if (pageClass) {
        page = [[pageClass alloc] init];
    }
    if ([page isKindOfClass:UIViewController.class]) {
        [self.navigationController pushViewController:page animated:YES];
        return;
    }
    
    // 若插件没有自己实现任何可跳转页面，且插件需要进行一些设置，则跳转到通用的设置页
    if (pluginInfo.hasSwitch) {
        ZCPDebugToolPlulginSettingsVC *settingsVC = [[ZCPDebugToolPlulginSettingsVC alloc] init];
        settingsVC.pluginInfo = pluginInfo;
        settingsVC.updateAction = ^{
            [self.pluginListView reloadData];
        };
        [self.navigationController pushViewController:settingsVC animated:YES];
    }
}

#pragma mark - getters and setters

- (void)setPluginInfos:(NSArray<ZCPDebugPluginInfo *> *)pluginInfos {
    _pluginInfos = [pluginInfos copy];
    [self.pluginListView reloadData];
}

- (UICollectionView *)pluginListView {
    if (!_pluginListView) {
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake((width - 40)/2, 60);
        layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
        
        _pluginListView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _pluginListView.backgroundColor = [UIColor whiteColor];
        _pluginListView.dataSource = self;
        _pluginListView.delegate = self;
        [_pluginListView registerClass:[_ZCPDebugToolPlulginListCell class] forCellWithReuseIdentifier:[_ZCPDebugToolPlulginListCell reuseIdentifier]];
    }
    return _pluginListView;
}

@end
