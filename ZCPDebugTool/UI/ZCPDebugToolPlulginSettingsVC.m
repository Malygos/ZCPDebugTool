//
//  ZCPDebugToolPlulginSettingsVC.m
//  ZCPDebugTool
//
//  Created by 朱超鹏 on 2021/3/13.
//

#import "ZCPDebugToolPlulginSettingsVC.h"

// Info Key
#define kSettingCellType    @"cellType"
#define kSettingSwitchType  @"switchType"
#define kSettingSwitchValue @"switchValue"
#define kSettingTitle       @"title"
#define kSettingSubTitle    @"subTitle"

// Cell Type
#define kSettingCellTypeSwitch  @"switch"
#define kSettingCellTypeInfo    @"info"

// Switch Type
#define kSettingSwitchTypeOn @"on"


@interface _ZCPDebugToolPlulginSettingCell : UITableViewCell
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UISwitch *settingSwitch;
@property (nonatomic, copy) NSString *switchType;
@property (nonatomic, copy) void(^switchAction)(_ZCPDebugToolPlulginSettingCell *cell, NSString *switchType);
@end
@implementation _ZCPDebugToolPlulginSettingCell
+ (NSString *)reuseIdentifier {
    return NSStringFromClass(self);
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.settingSwitch];
    }
    return self;
}
- (void)layoutSubviews {
    [super layoutSubviews];
    CGSize size = self.bounds.size;
    self.titleLabel.frame = CGRectMake(16, 0, size.width - 51 - 32, size.height);
    self.settingSwitch.frame = CGRectMake(size.width - 51 - 16, (size.height - 31) / 2, 51, 31);
}
- (void)switchValueDidChanged {
    if (self.switchAction) {
        self.switchAction(self, self.switchType);
    }
}
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.numberOfLines = 0;
        _titleLabel.font = [UIFont systemFontOfSize:15];
    }
    return _titleLabel;
}
- (UISwitch *)settingSwitch {
    if (!_settingSwitch) {
        _settingSwitch = [[UISwitch alloc] init];
        [_settingSwitch addTarget:self action:@selector(switchValueDidChanged) forControlEvents:UIControlEventValueChanged];
    }
    return _settingSwitch;
}
@end


@interface ZCPDebugToolPlulginSettingsVC () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *settings;

@end

@implementation ZCPDebugToolPlulginSettingsVC

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    self.settings = @[
        @{
            kSettingCellType: kSettingCellTypeSwitch,
            kSettingSwitchType: kSettingSwitchTypeOn,
            kSettingSwitchValue: @(self.pluginInfo.isOn),
            kSettingTitle: @"On/Off",
        },
        @{
            kSettingCellType: kSettingCellTypeInfo,
            kSettingTitle: @"Quick Access",
            kSettingSubTitle: @"Not support!"
        },
    ];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.tableView.frame = self.view.bounds;
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.settings.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *info = self.settings[indexPath.row];
    
    if ([info[kSettingCellType] isEqualToString:kSettingCellTypeSwitch]) {
        _ZCPDebugToolPlulginSettingCell *cell = [tableView dequeueReusableCellWithIdentifier:[_ZCPDebugToolPlulginSettingCell reuseIdentifier]];
        NSDictionary *info = self.settings[indexPath.row];
        cell.titleLabel.text = info[kSettingTitle];
        cell.settingSwitch.on = [info[kSettingSwitchValue] boolValue];
        cell.switchType = info[kSettingSwitchType];
        cell.switchAction = self.switchHandler;
        return cell;
    } else if ([info[kSettingCellType] isEqualToString:kSettingCellTypeInfo]) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(UITableViewCell.class)];
        cell.textLabel.text = info[kSettingTitle];
        cell.detailTextLabel.text = info[kSettingSubTitle];
        return cell;
    }
    return nil;
}

- (void(^)(_ZCPDebugToolPlulginSettingCell *, NSString *))switchHandler {
    return ^(_ZCPDebugToolPlulginSettingCell *cell, NSString *switchType) {
        if ([switchType isEqualToString:kSettingSwitchTypeOn]) {
            self.pluginInfo.on = cell.settingSwitch.isOn;
            if (self.updateAction) self.updateAction();
        }
    };
}

#pragma mark - getters

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        _tableView.rowHeight = 60.0;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView registerClass:[_ZCPDebugToolPlulginSettingCell class] forCellReuseIdentifier:[_ZCPDebugToolPlulginSettingCell reuseIdentifier]];
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:NSStringFromClass(UITableViewCell.class)];
    }
    return _tableView;
}

@end
