//
//  ZCPDebugToolFloatBallVC.m
//  Pods
//
//  Created by 朱超鹏 on 2021/3/7.
//

#import "ZCPDebugToolFloatBallVC.h"

#define kZCPDebugToolFloatBallDefaultReactKey @"kZCPDebugToolFloatBallDefaultReactKey"
#define ZCPDebugToolFloatBallSize 40

@interface ZCPDebugToolFloatBallVC ()

@property (nonatomic, strong) UIImageView *floatBallView;
@property (nonatomic, assign, readwrite) CGRect defaultBallRect;
@property (nonatomic, assign) BOOL panGestureAdded;

@end

@implementation ZCPDebugToolFloatBallVC

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.floatBallView];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.floatBallView.frame = self.view.bounds;
    
    if (self.view.window && !self.panGestureAdded) {
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
        [self.view.window addGestureRecognizer:pan];
        self.panGestureAdded = YES;
    }
}

#pragma mark - override

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}

#pragma mark - event response

- (void)pan:(UIPanGestureRecognizer *)panGestureRecognizer {
    //1、获得拖动位移
    CGPoint offsetPoint = [panGestureRecognizer translationInView:panGestureRecognizer.view];
    //2、清空拖动位移
    [panGestureRecognizer setTranslation:CGPointZero inView:panGestureRecognizer.view];
    //3、重新设置控件位置
    UIView *panView = panGestureRecognizer.view;
    CGFloat newX = panView.center.x + offsetPoint.x;
    CGFloat newY = panView.center.y + offsetPoint.y;
    if (newX < ZCPDebugToolFloatBallSize / 2) {
        newX = ZCPDebugToolFloatBallSize / 2;
    }
    if (newX > [UIScreen mainScreen].bounds.size.width - ZCPDebugToolFloatBallSize / 2) {
        newX = [UIScreen mainScreen].bounds.size.width - ZCPDebugToolFloatBallSize / 2;
    }
    if (newY < ZCPDebugToolFloatBallSize / 2) {
        newY = ZCPDebugToolFloatBallSize / 2;
    }
    if (newY > [UIScreen mainScreen].bounds.size.height - ZCPDebugToolFloatBallSize / 2) {
        newY = [UIScreen mainScreen].bounds.size.height - ZCPDebugToolFloatBallSize / 2;
    }
    panView.center = CGPointMake(newX, newY);
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:NSStringFromCGRect(panView.frame) forKey:kZCPDebugToolFloatBallDefaultReactKey];
}

#pragma mark - public method

- (void)show {
    self.view.window.hidden = NO;
}

- (void)singleTapped {
    self.view.window.hidden = YES;
    if (self.singleClickAction) {
        self.singleClickAction();
    }
}

- (void)longPress {
    self.view.window.hidden = YES;
    if (self.longPressAction) {
        self.longPressAction();
    }
}

#pragma mark - getters and setters

- (UIImageView *)floatBallView {
    if (!_floatBallView) {
        _floatBallView = [[UIImageView alloc] init];
        _floatBallView.backgroundColor = [UIColor cyanColor];
        _floatBallView.layer.cornerRadius = ZCPDebugToolFloatBallSize / 2;
        _floatBallView.layer.masksToBounds = YES;
        _floatBallView.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapped)];
        singleTap.numberOfTapsRequired = 1;
        [_floatBallView addGestureRecognizer:singleTap];
        
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress)];
        [_floatBallView addGestureRecognizer:longPress];
        
        [singleTap requireGestureRecognizerToFail:longPress];
    }
    return _floatBallView;
}

+ (CGRect)defaultBallRect {
    CGRect defaultBallRect = CGRectZero;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    CGRect cacheRect = CGRectFromString([userDefaults objectForKey:kZCPDebugToolFloatBallDefaultReactKey]);
    
    if (CGRectEqualToRect(cacheRect, CGRectZero)) {
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        defaultBallRect = CGRectMake(screenSize.width - ZCPDebugToolFloatBallSize, (screenSize.height - ZCPDebugToolFloatBallSize) / 2, ZCPDebugToolFloatBallSize, ZCPDebugToolFloatBallSize);
        [userDefaults setObject:NSStringFromCGRect(defaultBallRect) forKey:kZCPDebugToolFloatBallDefaultReactKey];
    } else {
        defaultBallRect = cacheRect;
    }
    
    return defaultBallRect;
}

@end
