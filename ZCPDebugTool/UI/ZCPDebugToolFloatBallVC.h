//
//  ZCPDebugToolFloatBallVC.h
//  Pods
//
//  Created by 朱超鹏 on 2021/3/7.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZCPDebugToolFloatBallVC : UIViewController

@property (nonatomic, copy) dispatch_block_t singleClickAction;
@property (nonatomic, copy) dispatch_block_t longPressAction;

+ (CGRect)defaultBallRect;

@end

NS_ASSUME_NONNULL_END
